#### 项目说明
这是一个使用pyside6（俗称PyQt)开发的GUI程序。通过该小程序的开发，可以学会如何使用pyside6开发跨平台的GUI程序。

该程序涉及的内容包含：

1. PyQt界面开发，使用自带designer画界面并转为python代码；

2. 使用cx_Oracle连接Oracle数据库并简单查询数据

3. excel文件的读取（xls和xlsx均支持）

4. 配置文件的读写


#### 依赖库
cx_Oracle
numpy
xlrd
xlwt
pyside6
openpyxl
xlutils

#### 如何根据ui文件生成python代码
```
python -c "import re; import sys; from PySide6.scripts.pyside_tool import uic; sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0]); uic();" .\demo.ui -o .\ui_mainwindow.py
```


#### 运行效果截图
Linux下截图

![linux](docs/linux.png)


windows下运行截图

![输入图片说明](docs/2.png)

![输入图片说明](docs/3.png)

![输入图片说明](docs/4.png)

![输入图片说明](docs/5.png)