import sys
sys.path.append('..')
from report_tool import get_prd_list, get_value_table, generate_excel, read_excel, get_trade_by_type
import logging

logging.basicConfig(
    filename = 'lczg.log',
    level = logging.DEBUG,
    format = "[%(asctime)s] - %(levelname)s - %(lineno)s] %(message)s",
    datefmt = "%Y-%m-%d %H:%M:%S"
)

def test_generate_excel():
    data_list = [{
        'prd_id': 'F16_04AF0263',
        'prd_name': '2020年封闭式净值型第26期C款',
        'prod_values': {
            'A0001': 35235.78, 
            'A2000': 0.0, 
            'A2100': 0.0, 
            'A4000': 0.0, 
            'A4400': 0.0, 
            'A4500': 0.0, 
            'A4700': 0.0, 
            'A5000': 0.0, 
            'A5100': 0.0, 
            'A7000': 0.0, 
            'A7200': 0.0, 
            'A7240': 0.0, 
            'A9000': 35235.78, 
            'D0000': 35235.78, 
            'B0000': 1426.18, 
            'B4000': 1426.18, 
            'C0000': 52846959.15, 
            'C1000': 0, 
            'C1200': 0, 
            'C1210': 0, 
            'C3000': 52846959.15
        }
    }]
    val_date = '2021-11-30'
    output = os.path.join(os.getcwd(), '123.xls')
    generate_excel(data_list, val_date, output)


def test_read_excel():
    path = 'D:/LCZG/新版理财资管/sunbb/learn-pyside6/债券台账.xls'
    trade_list = read_excel(path)
    print(trade_list)
    

def test_get_value_table():
    prd_no = '04AF0324'
    value_date = '20211130'
    value_table = get_value_table(prd_no, value_date)
    print(value_table)
    
    

def test_get_prd_list():
    value_date = '20211130'
    res = get_prd_list(value_date)
    print(res)


def test_log():
    data = [1,2,3,4,5]
    s = '123.4xt'
    #logging.info('hello', s)
    logging.info(data)


if __name__ == '__main__':
    test_log()
    #test_read_excel()
    #test_generate_excel()
    #test_get_prd_list()
    #test_get_value_table()
    #test_prd_detail()
