from PySide6.QtWidgets import (QApplication, QLabel, QMainWindow, QMenuBar,
    QPushButton, QSizePolicy, QStatusBar, QTextEdit,
    QWidget, QVBoxLayout, QStyleFactory, QDialog, QFrame, 
    QHeaderView, QAbstractItemView, QTableWidgetItem, QCheckBox, QHBoxLayout,
    QFileDialog, QMessageBox)
from ui.ui_dlg_choose_date import Ui_dlgChooseDate

class DlgChooseDate(QDialog):
    def __init__(self):
        self.date_str = None
        super(DlgChooseDate, self).__init__()
        self.ui = Ui_dlgChooseDate()
        self.ui.setupUi(self)
        self.setWindowTitle("请选择净值日期")
        self.ui.calendarWidget.clicked.connect(self.chooseDate)
        self.ui.btnAcceptDate.clicked.connect(self.quitDlg)
        self.ui.btnCancelDate.clicked.connect(self.quitDlg)
    
    def chooseDate(self, date):
        print(date.toString('yyyy-MM-dd'))
        self.date_str = date.toString('yyyy-MM-dd')
    def getDate(self):
        return self.date_str
    def quitDlg(self):
        self.close()
