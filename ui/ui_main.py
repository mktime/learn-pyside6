# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGroupBox, QHBoxLayout, QHeaderView,
    QLabel, QLayout, QLineEdit, QMainWindow,
    QPushButton, QSizePolicy, QStatusBar, QTabWidget,
    QTableWidget, QTableWidgetItem, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(803, 600)
        MainWindow.setFocusPolicy(Qt.NoFocus)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setGeometry(QRect(0, 10, 801, 551))
        self.importTrades = QWidget()
        self.importTrades.setObjectName(u"importTrades")
        self.groupBox = QGroupBox(self.importTrades)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(3, 3, 790, 41))
        self.btnImportTrades = QPushButton(self.groupBox)
        self.btnImportTrades.setObjectName(u"btnImportTrades")
        self.btnImportTrades.setGeometry(QRect(20, 10, 75, 24))
        self.tableWidgetTrades = QTableWidget(self.importTrades)
        self.tableWidgetTrades.setObjectName(u"tableWidgetTrades")
        self.tableWidgetTrades.setGeometry(QRect(0, 49, 801, 481))
        self.tabWidget.addTab(self.importTrades, "")
        self.exportValueTable = QWidget()
        self.exportValueTable.setObjectName(u"exportValueTable")
        self.horizontalLayoutWidget = QWidget(self.exportValueTable)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(0, 0, 801, 51))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setSpacing(24)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.horizontalLayout.setContentsMargins(12, 0, 12, 0)
        self.lable = QLabel(self.horizontalLayoutWidget)
        self.lable.setObjectName(u"lable")

        self.horizontalLayout.addWidget(self.lable)

        self.labelDate = QLabel(self.horizontalLayoutWidget)
        self.labelDate.setObjectName(u"labelDate")

        self.horizontalLayout.addWidget(self.labelDate)

        self.btnChoseDate = QPushButton(self.horizontalLayoutWidget)
        self.btnChoseDate.setObjectName(u"btnChoseDate")

        self.horizontalLayout.addWidget(self.btnChoseDate)

        self.btnExport = QPushButton(self.horizontalLayoutWidget)
        self.btnExport.setObjectName(u"btnExport")

        self.horizontalLayout.addWidget(self.btnExport)

        self.btnExit = QPushButton(self.horizontalLayoutWidget)
        self.btnExit.setObjectName(u"btnExit")

        self.horizontalLayout.addWidget(self.btnExit)

        self.tableWidget = QTableWidget(self.exportValueTable)
        self.tableWidget.setObjectName(u"tableWidget")
        self.tableWidget.setGeometry(QRect(0, 60, 801, 471))
        self.tabWidget.addTab(self.exportValueTable, "")
        self.settings = QWidget()
        self.settings.setObjectName(u"settings")
        self.groupBox_3 = QGroupBox(self.settings)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.groupBox_3.setGeometry(QRect(0, 10, 791, 521))
        self.txtJDBC = QLineEdit(self.groupBox_3)
        self.txtJDBC.setObjectName(u"txtJDBC")
        self.txtJDBC.setGeometry(QRect(110, 26, 231, 21))
        self.label = QLabel(self.groupBox_3)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 30, 71, 16))
        self.txtUserName = QLineEdit(self.groupBox_3)
        self.txtUserName.setObjectName(u"txtUserName")
        self.txtUserName.setGeometry(QRect(110, 60, 231, 21))
        self.label_2 = QLabel(self.groupBox_3)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(20, 64, 71, 16))
        self.txtPasswd = QLineEdit(self.groupBox_3)
        self.txtPasswd.setObjectName(u"txtPasswd")
        self.txtPasswd.setGeometry(QRect(110, 90, 231, 21))
        self.label_3 = QLabel(self.groupBox_3)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(20, 94, 71, 16))
        self.btnSaveConfig = QPushButton(self.groupBox_3)
        self.btnSaveConfig.setObjectName(u"btnSaveConfig")
        self.btnSaveConfig.setGeometry(QRect(270, 170, 75, 24))
        self.label_4 = QLabel(self.groupBox_3)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(20, 130, 81, 16))
        self.txtOracleHome = QLineEdit(self.groupBox_3)
        self.txtOracleHome.setObjectName(u"txtOracleHome")
        self.txtOracleHome.setGeometry(QRect(110, 126, 231, 21))
        self.tabWidget.addTab(self.settings, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.groupBox.setTitle("")
        self.btnImportTrades.setText(QCoreApplication.translate("MainWindow", u"\u5bfc\u5165", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.importTrades), QCoreApplication.translate("MainWindow", u"\u5bfc\u5165\u503a\u5238\u5206\u7c7b", None))
        self.lable.setText(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Microsoft YaHei UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">\u51c0\u503c\u65e5\u671f\uff1a</span></p></body></html>", None))
        self.labelDate.setText("")
        self.btnChoseDate.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u51c0\u503c\u65e5\u671f", None))
        self.btnExport.setText(QCoreApplication.translate("MainWindow", u"\u5bfc\u51fa\u5df2\u9009\u8bb0\u5f55", None))
        self.btnExit.setText(QCoreApplication.translate("MainWindow", u"\u9000\u51fa", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.exportValueTable), QCoreApplication.translate("MainWindow", u"\u5bfc\u51fa\u4fe1\u6258\u62a5\u8868", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("MainWindow", u"\u6570\u636e\u5e93\u8bbe\u7f6e", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"\u6570\u636e\u5e93\u5730\u5740\uff1a", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"\u7528\u6237\u540d\uff1a", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"\u5bc6\u7801\uff1a", None))
        self.btnSaveConfig.setText(QCoreApplication.translate("MainWindow", u"\u4fdd\u5b58", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"oracle_home\uff1a", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.settings), QCoreApplication.translate("MainWindow", u"\u8bbe\u7f6e", None))
    # retranslateUi

