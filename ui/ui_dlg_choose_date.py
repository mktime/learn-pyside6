# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'DlgChooseDate.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCalendarWidget, QDialog, QGridLayout,
    QHBoxLayout, QPushButton, QSizePolicy, QWidget)

class Ui_dlgChooseDate(object):
    def setupUi(self, dlgChooseDate):
        if not dlgChooseDate.objectName():
            dlgChooseDate.setObjectName(u"dlgChooseDate")
        dlgChooseDate.resize(282, 228)
        self.gridLayoutWidget = QWidget(dlgChooseDate)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(0, 0, 281, 222))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(4, 0, 4, 0)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(18)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(-1, 0, -1, -1)
        self.btnAcceptDate = QPushButton(self.gridLayoutWidget)
        self.btnAcceptDate.setObjectName(u"btnAcceptDate")

        self.horizontalLayout.addWidget(self.btnAcceptDate)

        self.btnCancelDate = QPushButton(self.gridLayoutWidget)
        self.btnCancelDate.setObjectName(u"btnCancelDate")

        self.horizontalLayout.addWidget(self.btnCancelDate)


        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 1)

        self.calendarWidget = QCalendarWidget(self.gridLayoutWidget)
        self.calendarWidget.setObjectName(u"calendarWidget")

        self.gridLayout.addWidget(self.calendarWidget, 0, 0, 1, 1)


        self.retranslateUi(dlgChooseDate)

        QMetaObject.connectSlotsByName(dlgChooseDate)
    # setupUi

    def retranslateUi(self, dlgChooseDate):
        dlgChooseDate.setWindowTitle(QCoreApplication.translate("dlgChooseDate", u"Dialog", None))
        self.btnAcceptDate.setText(QCoreApplication.translate("dlgChooseDate", u"\u786e\u5b9a", None))
        self.btnCancelDate.setText(QCoreApplication.translate("dlgChooseDate", u"\u53d6\u6d88", None))
    # retranslateUi

